 import { LitElement, html } from 'lit-element';

 class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        //Llamar siempre a super() primero
        super();

        this.name= "Prueba nombre";
        this.yearsInCompany= 14;
        this.photo= {
            src: "./img/persona.jpg",
            alt: "foto persona"
        };

        this.updatePersonInfo();
    }

    render() {
        return html`
         <div>
            <label for="fname">Nombre Completo</label>
            <input type="text" id="fname" name="fname" value="${this.name}" @change="${this.updateName}"/>
            <br />
            <label for="yearsInCompany">Años en la empresa</label>
            <input type="text" id="yearsInCompany" name="yearsInCompany" value="${this.yearsInCompany}"  @input="${this.updateYearsInCompany}" />
            <br />
            <input type="text" id="personInfo" name="personInfo" value="${this.personInfo}" disabled />
            <br />
            <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}" >
            <br />
         </div>
        `;   
    }

    //changedProperties is a Map
    updated(changedProperties) {
        console.log("updated");
        if (changedProperties.has("name")) {
            console.log("Propiedad name cambiada anterior era " + changedProperties.get("name") + " nuevo es " + this.name)
        }
        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambiada anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }
    
    updateName(e) {
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        console.log("yearsInCompany vale " + this.yearsInCompany);

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }
 }
 customElements.define('ficha-persona', FichaPersona)